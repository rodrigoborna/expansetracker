import { NavigationContainer } from '@react-navigation/native';
import DrawerNavigation from './src/components/DrawerNavigation';
import React from 'react';
import * as SplashScreen from 'expo-splash-screen';
import {
  Image,
  View,
  Text
} from 'react-native';

export default class App extends React.Component {
  state = {
    isLoggedValidate:false,
    appIsReady: false
  };

  async componentDidMount() {
    // Prevent native splash screen from autohiding
    try {
      await SplashScreen.preventAutoHideAsync();
      SplashScreen.hideAsync({ immediate: true });
      this.prepareResources();
      
    } catch (e) {
      console.warn(e);
    }
    
    
  }

  /**
   * Method that serves to load resources and make API calls
   */
  prepareResources = async () => {
    setTimeout(() => this.setState({ appIsReady: true, isLoggedValidate: false }), 3000)  
  
  }
  render() {
    if (!this.state.appIsReady) {
      return (
        <View style={{backgroundColor: '#EB822F', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
       
          <View>
            <Image
              source={require('./src/images/baby-yoda.png')}
              style={{width:380,height:380,resizeMode: "contain"}}
            // onLoad={this._cacheResourcesAsync}
            />
          </View>
          <Text style={{ fontWeight: 'bold', fontSize: 22, alignItems: 'center' }}>Seu dinheiro controlar, você deve!</Text>
          <View>
            <Image style={{ width: 60, height: 60 }}
              source={require('./src/images/loading.gif')}
            // onLoad={this._cacheResourcesAsync}
            />
          </View>
        </View>
      )
    }

    return (

      <NavigationContainer>
        <DrawerNavigation />
      </NavigationContainer>

    )
  }
}
