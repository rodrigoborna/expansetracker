import React, { useState } from 'react';
import { TouchableOpacity, View, Text, TextInput, StyleSheet } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';



export default function RecordMovimentModal(props) {
  DropDownPicker.setListMode("SCROLLVIEW");
  DropDownPicker.setMode("BADGE");
  const modalizeRef = props.modalize;
  const [openType, setOpenType] = useState(false);
  const [openOptions, setOpenOptions] = useState(false);
  const [openAccount, setOpenAccount] = useState(false);
  const [openTag, setOpenTag] = useState(false);
  const [valueType, setValueType] = useState(null);
  const [valueOption, setValueOption] = useState(null);
  const [valueAccount, setValueAccount] = useState(null);
  const [valueTag, setValueTag] = useState([]);
  const [types, setTypes] = useState([
    { label: 'Receita', value: 'receita' },
    { label: 'Despesa', value: 'despesa' }
  ]);
  const [options, setOptions] = useState([
    { label: 'Sim', value: 'sim' },
    { label: 'Não', value: 'não' }
  ]);
  const [account, setAccount] = useState([
    { label: 'Bradesco', value: 'bradesco' },
    { label: 'Itaú', value: 'itau' },
    { label: 'NuBank', value: 'nubank' },
    { label: 'Inter', value: 'inter' }
  ]);
  const [tag, setTag] = useState([
    { label: 'Combustível', value: 'combustivel' },
    { label: 'Viagem', value: 'viagem' },
    { label: 'Aniversário', value: 'aniversario' },
    { label: 'IPVA', value: 'ipva' }
  ]);
  const handleClose = () => {
    if (modalizeRef.current) {
      modalizeRef.current.close();
    }
  };
  return (
    <View style={styles.content}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={styles.content__subheading}>{'Lançamento Financeiro'.toUpperCase()}</Text>
        <TouchableOpacity onPress={handleClose}>
          <Ionicons name="close" size={25} color={'#B7460C'} />
        </TouchableOpacity>
      </View>

      <View>
        <Text style={styles.content__heading}>Descrição</Text>
        <TextInput style={styles.content__input} />
      </View>

      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
        <View>
          <Text style={styles.content__heading}>Valor</Text>
          <TextInput style={styles.content__input2} />
        </View>
        <View>
          <Text style={styles.content__heading}>Data</Text>
          <TextInput style={styles.content__input2} />
        </View>
      </View>

      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between',marginBottom:10 }}>
        <View>
          <Text style={styles.content__heading}>Tipo</Text>
          <DropDownPicker
            style={{height:30,width: 150,borderColor:'#A9A9A9',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0}}
            open={openType}
            value={valueType}
            items={types}
            setOpen={setOpenType}
            setValue={setValueType}
            setItems={setTypes}
            textStyle={{ fontSize: 18, color:'#808080' }}
            placeholder="---"
            searchable={true}
            searchPlaceholder="Procurar"
            dropDownDirection="TOP"
          />
        </View>
        <View>
          <Text style={styles.content__heading}>Fixa</Text>
          <DropDownPicker
            style={{height:30,width: 150,borderColor:'#A9A9A9',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0}}
            open={openOptions}
            value={valueOption}
            items={options}
            setOpen={setOpenOptions}
            setValue={setValueOption}
            setItems={setOptions}
            textStyle={{ fontSize: 18, color:'#808080' }}
            placeholder="---"
            searchable={true}
            searchPlaceholder="Procurar"
            dropDownDirection="TOP"
          />
        </View>
      </View>

      <View style={{ marginBottom: 10 }}>
        <Text style={styles.content__heading}>Conta</Text>
        <DropDownPicker
          style={{height:30,borderColor:'#A9A9A9',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0}}
          open={openAccount}
          value={valueAccount}
          items={account}
          setOpen={setOpenAccount}
          setValue={setValueAccount}
          setItems={setAccount}
          textStyle={{ fontSize: 18, color:'#808080' }}
          placeholder="---"
          searchable={true}
          searchPlaceholder="Procurar"
          dropDownDirection="TOP"
        />
      </View>

      <View>
        <Text style={styles.content__heading}>Tag</Text>
        <DropDownPicker
          style={{height:40,borderColor:'#A9A9A9',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0}}
          multiple={true}
          open={openTag}
          value={valueTag}
          items={tag}
          setOpen={setOpenTag}
          setValue={setValueTag}
          setItems={setTag}
          textStyle={{ fontSize: 18, color:'#808080' }}
          placeholder="---"
          searchable={true}
          searchPlaceholder="Procurar"
          dropDownDirection="TOP"
          categorySelectable={true}
          />
      </View>


      <View style={{ marginBottom: 15 }}></View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <TouchableOpacity style={styles.content__buttonEdit} activeOpacity={0.75} onPress={() => console.log(valueType, valueOption)}>
          <Text style={styles.content__buttonText}>{'Cadastrar'.toUpperCase()}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.content__buttonDelete} activeOpacity={0.75} onPress={handleClose}>
          <Text style={styles.content__buttonText}>{'Cancelar'.toUpperCase()}</Text>
        </TouchableOpacity>
      </View>
    </View>

  );
}

const styles = StyleSheet.create(
  {
    content: {
      padding: 20,
    },

    content__icon: {
      width: 32,
      height: 32,

      marginBottom: 20,
    },

    content__subheading: {
      marginBottom: 10,

      fontSize: 16,
      fontWeight: '600',
      color: '#B7460C',
    },

    content__heading: {
      fontSize: 18,
      fontWeight: '600',
      color: '#808080',
      paddingLeft: 5,
      fontWeight: 'bold',
      marginBottom: 5
    },

    content__description: {
      fontSize: 15,
      fontWeight: '200',
      lineHeight: 22,
      color: '#666',

    },

    content__input: {
      width: '100%',
      borderBottomColor: '#A9A9A9',
      borderBottomWidth: 1,
      paddingLeft: 5,
      marginBottom: 20,
      fontSize: 20,
      color:'#808080'
    },
    content__input2: {
      flex: 0.5,
      width: 150,
      borderBottomColor: '#A9A9A9',
      borderBottomWidth: 1,
      paddingLeft: 5,
      marginBottom: 20,
      fontSize: 18,
      color:'#808080'
    },

    content__buttonEdit: {
      paddingVertical: 15,
      width: '50%',
      borderRightWidth:1,
      borderRightColor:'#808080'
      //backgroundColor: '#333',
      //borderRadius: 6,
    },
    content__buttonDelete: {
      paddingVertical: 15,
      width: '50%',
      // backgroundColor: '#FF7777',
      // borderRadius: 6,
    },

    content__buttonText: {
      color: '#B7460C',
      fontSize: 15,
      fontWeight: '600',
      textAlign: 'center',
    },

  }
);