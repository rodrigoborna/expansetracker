import React, { useState } from 'react';
import { TouchableOpacity,View,Text,TextInput,StyleSheet} from 'react-native';
import { Ionicons} from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';



export default function DetailsTransactionModal(props) {
    const modalizeRef = props.modalize;
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Receita', value: 'receita' },
        { label: 'Despesa', value: 'despesa' }
    ]);
    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };
    return (
      <View style={styles.content}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.content__subheading}>{'Detalhes da Transação'.toUpperCase()}</Text>
          <TouchableOpacity onPress={handleClose}>
              <Ionicons name="close" size={25} color={'#A9A9A9'} />
          </TouchableOpacity>
      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
          <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="wallet" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Descrição</Text>
                  <Text style={styles.content__description}>Compra de XPTO</Text>
              </View>
          </View>
      </View>
      
      <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
          <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="bookmark" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Categoria</Text>
                  <Text style={styles.content__description}>Gastos Diversos</Text>
              </View>
          </View>
      </View>


      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="cash" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Valor</Text>
                  <Text style={styles.content__description}>- R$ 100.000,00</Text>
              </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="calendar" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Data</Text>
                  <Text style={styles.content__description}>01/01/2021</Text>
              </View>
          </View>
          
      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="wallet" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Conta</Text>
                  <Text style={styles.content__description}>Nubank da Silva Sauro</Text>
              </View>
          </View>
          <View style={{ flexDirection: 'row',marginRight:45 }}>
              <View style={{ marginTop: 8}}>
                  <Ionicons name="checkbox-outline" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Fixa</Text>
                  <Text style={styles.content__description}>Não</Text>
              </View>
          </View>
          

      </View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="card" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Modo Pagto.</Text>
                  <Text style={styles.content__description}>À Prazo</Text>
              </View>
          </View>
          <View style={{ flexDirection: 'row'}}>
              <View style={{ marginTop: 8 }}>
                  <Ionicons name="copy" size={25} color={'#A9A9A9'} />
              </View>
              <View style={{ marginLeft: 15 }}>
                  <Text style={styles.content__heading}>Parcelas</Text>
                  <Text style={styles.content__description}>5</Text>
              </View>
          </View>
      </View>

      

     

      <View style={{ marginBottom: 15 }}></View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.content__subheading}>{'Detalhes das Próximas 3 Parcelas'.toUpperCase()}</Text>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.content__heading}>Parcela 1</Text>
          <Text style={styles.content__heading}><Text style={{ fontWeight: 'normal' }}>01/02/2021</Text></Text>
          <Text style={styles.content__heading}><Text style={{ fontWeight: 'normal' }}>R$ 100,00</Text></Text>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.content__heading}>Parcela 2</Text>
          <Text style={styles.content__heading}><Text style={{ fontWeight: 'normal' }}>01/03/2021</Text></Text>
          <Text style={styles.content__heading}><Text style={{ fontWeight: 'normal' }}>R$ 100,00</Text></Text>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={styles.content__heading}>Parcela 3</Text>
          <Text style={styles.content__heading}><Text style={{ fontWeight: 'normal' }}>01/04/2021</Text></Text>
          <Text style={styles.content__heading}><Text style={{ fontWeight: 'normal' }}>R$ 100,00</Text></Text>
      </View>

      <View style={{ marginBottom: 15 }}></View>

      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <TouchableOpacity style={styles.content__buttonEdit} activeOpacity={0.75} onPress={handleClose}>
              <Text style={styles.content__buttonText}>{'Editar'.toUpperCase()}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.content__buttonDelete} activeOpacity={0.75} onPress={handleClose}>
              <Text style={styles.content__buttonText}>{'Excluir'.toUpperCase()}</Text>
          </TouchableOpacity>
      </View>
  </View>


    );
}

const styles=StyleSheet.create(
  {
    content: {
      padding: 20,
    },
  
    content__icon: {
      width: 32,
      height: 32,
  
      marginBottom: 20,
    },
  
    content__subheading: {
      marginBottom: 2,
  
      fontSize: 16,
      fontWeight: '600',
      color: '#B7460C',
    },
  
    content__heading: {
      fontSize: 20,
      fontWeight: '600',
      color: '#808080',
      paddingTop: 5,
      fontWeight:'bold'
    },
  
    content__description: { 
      fontSize: 15,
      fontWeight: '200',
      lineHeight: 22,
      color: '#808080',
    },
  
    content__input: {
      paddingVertical: 15,
      marginBottom: 20,
  
      width: '100%',
  
      borderWidth: 1,
      borderColor: 'transparent',
      borderBottomColor: '#cdcdcd',
      borderRadius: 6,
    },
  
    content__buttonEdit: {
        paddingVertical: 15,
        width: '50%',
        borderRightWidth:1,
        borderRightColor:'#808080'

      },
      content__buttonDelete: {
        paddingVertical: 15,
        width: '50%',
      },
  
      content__buttonText: {
        color: '#B7460C',
        fontSize: 15,
        fontWeight: '600',
        textAlign: 'center',
      },

  }
);