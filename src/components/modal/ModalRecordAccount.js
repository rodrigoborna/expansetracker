import React, { useState } from 'react';
import { TouchableOpacity,View,Text,TextInput,StyleSheet} from 'react-native';
import { Ionicons} from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';



export default function RecordAccountModal(props) {
    DropDownPicker.setListMode("SCROLLVIEW");
    const modalizeRef = props.modalize;
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Receita', value: 'receita' },
        { label: 'Despesa', value: 'despesa' }
    ]);
    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };
    return (
        <View style={styles.content}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={styles.content__subheading}>{'Cadastro de Conta'.toUpperCase()}</Text>
                <TouchableOpacity onPress={handleClose}>
                    <Ionicons name="close" size={25} color={'#B7460C'} />
                </TouchableOpacity>
            </View>

            <View>
                <Text style={styles.content__heading}>Descrição</Text>
                <TextInput style={styles.content__input} />
            </View>

            <View style={{ marginBottom: 15 }}></View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <TouchableOpacity style={styles.content__buttonEdit} activeOpacity={0.75} onPress={() => console.log(value)}>
                    <Text style={styles.content__buttonText}>{'Cadastrar'.toUpperCase()}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.content__buttonDelete} activeOpacity={0.75} onPress={handleClose}>
                    <Text style={styles.content__buttonText}>{'Cancelar'.toUpperCase()}</Text>
                </TouchableOpacity>
            </View>
        </View>

    );
}

const styles = StyleSheet.create(
    {
      content: {
        padding: 20,
      },
    
      content__icon: {
        width: 32,
        height: 32,
    
        marginBottom: 20,
      },
    
      content__subheading: {
        marginBottom: 10,
        fontSize: 16,
        fontWeight: '600',
        color: '#B7460C',
      },
    
      content__heading: {
        fontSize: 18,
        fontWeight: '600',
        color: '#808080',
        paddingLeft:5,
        fontWeight:'bold',
        marginBottom:5
      },
    
      content__description: { 
        fontSize: 15,
        fontWeight: '200',
        lineHeight: 22,
        color: '#666',
        
      },
    
      content__input: {
        width: '100%',
        borderBottomColor: '#A9A9A9',
        borderBottomWidth:1,
        paddingLeft:5,
        marginBottom:20,
        fontSize:18,
        color:'#808080'
      },
    
      content__buttonEdit: {
        paddingVertical: 15,
        width: '50%',
        borderRightWidth:1,
        borderRightColor:'#808080'
        //backgroundColor: '#333',
        //borderRadius: 6,
      },
      content__buttonDelete: {
        paddingVertical: 15,
        width: '50%',
        // backgroundColor: '#FF7777',
        // borderRadius: 6,
      },
    
      content__buttonText: {
        color: '#B7460C',
        fontSize: 15,
        fontWeight: '600',
        textAlign: 'center',
      },
  
    }
  );