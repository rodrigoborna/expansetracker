import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import ProfileScreen from '../screens/profile';
import DashboardScreen from '../screens/dashboard';
import TransactionsScreen from '../screens/transactions';
import TransactionsScreen2 from '../screens/transactions2';
import LoginScreen from '../screens/login';
import RegisterScreen from '../screens/register';
import RecordsScreen from '../screens/records';
import CategoryScreen from '../screens/category';
import AccountScreen from '../screens/account';
import SidebarNav from './SidebarNav';



const Drawer = createDrawerNavigator();

export default function DrawerNavigation() {
    return (

<Drawer.Navigator drawerContent={props =><SidebarNav {...props}/>} >
        <Drawer.Screen name="Home" component={DashboardScreen} />
        <Drawer.Screen name="Profile" component={ProfileScreen} />
        <Drawer.Screen name="Transactions" component={TransactionsScreen} />
        <Drawer.Screen name="Transactions2" component={TransactionsScreen2} />
        <Drawer.Screen name="Login" component={LoginScreen} />
        <Drawer.Screen name="Register" component={RegisterScreen} />
        <Drawer.Screen name="Cadastros" component={RecordsScreen} />
        <Drawer.Screen name="Categorias" component={CategoryScreen} />
        <Drawer.Screen name="Contas" component={AccountScreen} />     
    </Drawer.Navigator>

 );
}

