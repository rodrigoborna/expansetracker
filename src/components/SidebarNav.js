import React,{useState,useEffect}from 'react';
import { View, Text, StyleSheet, ImageBackground, SafeAreaView } from 'react-native';
//import { MaterialIcons} from "@expo/vector-icons";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
    Avatar,
    Title,
    Caption,
    Drawer,
} from 'react-native-paper';

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';


export default function SidebarNav(props) {

    return (
        <SafeAreaView style={{ flex: 1 }}>
            
            <View style={{ flex: 0.2 }}>
                <ImageBackground source={require("../images/Money.jpg")} style={{ flex: 1, position: 'relative', height: 120 }} >
                    <View style={styles.userInfoSection}>
                        <View style={{ flexDirection: 'row', marginTop: 40 }}>
                            <Avatar.Image
                                source={require('../images/profile-pic2.jpg')}
                                size={50}
                            />
                            <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                                <Title style={styles.title}>Rodrigo</Title>
                                <Caption style={styles.caption}>teste@teste.com</Caption>
                            </View>
                        </View>

                        {/* <View style={styles.row}>
                        <View style={styles.section}>
                            <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
                            <Caption style={styles.caption}></Caption>
                        </View>
                        <View style={styles.section}>
                            <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
                            <Caption style={styles.caption}>Followers</Caption>
                        </View>
                    </View> */}

                    </View>
                </ImageBackground>
            </View>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>

                    <Drawer.Section title="Menus" >
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="home"
                                    color={"blue"}
                                    size={size}
                                />
                            )}
                            label="Home"
                            onPress={() => { props.navigation.navigate('Home') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="face"
                                    color={"steelblue"}
                                    size={size}
                                />
                            )}
                            label="Profile"
                            onPress={() => { props.navigation.navigate('Profile') }}
                        />
                       
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="dashboard"
                                    color={"brown"}
                                    size={size}
                                />
                            )}
                            label="Cadastros"
                            onPress={() => { props.navigation.navigate('Cadastros') }}
                        />
                         <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="bookmark"
                                    color={"brown"}
                                    size={size}
                                />
                            )}
                            label="Categorias"
                            onPress={() => { props.navigation.navigate('Categorias') }}
                        />
                         <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="credit-card"
                                    color={"brown"}
                                    size={size}
                                />
                            )}
                            label="Contas"
                            onPress={() => { props.navigation.navigate('Contas') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="list"
                                    color={"#483D8B"}
                                    size={size}
                                />
                            )}
                            label="Movimento"
                            onPress={() => { props.navigation.navigate('Transactions2') }}
                        />

                    </Drawer.Section>
                    <Drawer.Section title="Inserções">
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="trending-up"
                                    color={"green"}
                                    size={size}
                                />
                            )}
                            label="Receita"
                            onPress={() => { props.navigation.navigate('') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="trending-down"
                                    color={"red"}
                                    size={size}
                                />
                            )}
                            label="Despesa"
                            onPress={() => { props.navigation.navigate('') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialIcons
                                    name="credit-card"
                                    color={"orange"}
                                    size={size}
                                />
                            )}
                            label="Cartão"
                            onPress={() => { props.navigation.navigate('') }}
                        />
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem
                    icon={({ color, size }) => (
                        <MaterialIcons
                            name="exit-to-app"
                            color={"purple"}
                            size={size}
                        />
                    )}
                    label="Sign Out"
                    onPress={() => { props.navigation.navigate('Login') }}
                />
            </Drawer.Section>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: "#fff"
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
        color: "#fff"
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        marginTop: 15,
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 3
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 3
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
});
