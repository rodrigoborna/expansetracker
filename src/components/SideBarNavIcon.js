import React from 'react';
import { TouchableOpacity,StyleSheet} from 'react-native';
import { Ionicons} from "@expo/vector-icons";

const SideBarNavIcon = (props) => {

    return (
        <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
          {/* </TouchableOpacity><TouchableOpacity onPress={() => console.log(props)}> */}
          <Ionicons name="menu" size={35} color="#B7460C" />
        </TouchableOpacity>
    );
}


export default SideBarNavIcon;