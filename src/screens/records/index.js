import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import SideBarNavIcon from '../../components/SideBarNavIcon';
import RecordCategoryModal from '../../components/modal/ModalRecordCategory';
import RecordSubCategoryModal from '../../components/modal/ModalRecordSubCategory';
import RecordAccountModal from '../../components/modal/ModalRecordAccount';
import RecordTagModal from '../../components/modal/ModalRecordTag';
import RecordMovimentModal from '../../components/modal/ModalRecordMoviment';
import { Modalize } from 'react-native-modalize';

export default function Records({ navigation }) {
    const [modal, setModal] = useState(null);
    const [toggle, setToggle] = useState(true);
    const modalizeRef = React.createRef();;

    const onOpen = (modalOpen) => {
        setModal(modalOpen)
        modalizeRef.current.open();
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#F5E4CA' }}>

            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={{ flex: 0.97 }}>
                    <Text style={{ marginLeft: 15, color: '#B7460C', fontSize: 25, fontWeight: 'bold', justifyContent: 'flex-start' }}>Cadastros</Text>
                </View>
                <View>
                    <SideBarNavIcon style={{ justifyContent: 'flex-end' }} navigation={navigation} />
                </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 0.33, flexDirection: 'row' }}>
                    <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => onOpen('category')} style={{
                            width: '90%',
                            height: '90%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={50} color={'#B7460C'} />
                                <Text style={{ fontSize: 18 }}>Categoria</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => onOpen('subcategory')} style={{
                            width: '90%',
                            height: '90%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmarks" size={50} color={'#B7460C'} />
                                <Text style={{ fontSize: 18 }}>Subcategoria</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 0.33, flexDirection: 'row' }}>
                    <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => onOpen('account')} style={{
                            width: '90%',
                            height: '90%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="wallet" size={50} color={'#B7460C'} />
                                <Text style={{ fontSize: 18 }}>Conta</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => onOpen('tag')} style={{
                            width: '90%',
                            height: '90%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="pricetag" size={50} color={'#B7460C'} />
                                <Text style={{ fontSize: 18 }}>Tag</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 0.33, flexDirection: 'row' }}>
                    <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => onOpen('moviment')} style={{
                            width: '90%',
                            height: '90%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
<Ionicons name="trending-up" size={50} color={'#B7460C'} />
<Text style={{ fontSize: 18 }}>Lançamento</Text>
</View>
                            </TouchableOpacity>
                    </View>
                    <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}>
                        
                    </View>
                </View>




            </View>


            <Modalize ref={modalizeRef} adjustToContentHeight={toggle}>
                {modal == 'category' && (<RecordCategoryModal modalize={modalizeRef} />)}
                {modal == 'subcategory' && (<RecordSubCategoryModal modalize={modalizeRef} />)}
                {modal == 'account' && (<RecordAccountModal modalize={modalizeRef} />)}
                {modal == 'tag' && (<RecordTagModal modalize={modalizeRef} />)}
                {modal == 'moviment' && (<RecordMovimentModal modalize={modalizeRef} />)}
                {modal == null && (null)}

            </Modalize>
        </View>
    );
}