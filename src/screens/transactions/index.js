import React, { useState } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import { trendingCurrencies } from '../../constants/dummy'
import SideBarNavIcon from '../../components/SideBarNavIcon';
import { Modalize } from 'react-native-modalize';
import DetailsTransactionModal from '../../components/modal/ModalDetailsTransaction';
import {Agenda} from 'react-native-calendars';

export default function Transactions({ navigation }) {
    const [trending, setTrending] = React.useState(trendingCurrencies);
    const [toggle, setToggle] = useState(true);
    const [items, setItems] = useState({});
    const modalizeRef = React.createRef();;
    const onOpen = (item) => {
        console.log(item.id)
        modalizeRef.current.open();
    };

    const renderItem = ({ item, index }) => (

        <TouchableOpacity
            style={{
                width: '95%',
                height: 60,
                padding: 5,
                marginLeft: 10,
                marginBottom: 10,
                borderRadius: 10,
                backgroundColor: '#F5E4CA',
                borderWidth: 3,
                borderColor: '#B7460C'
            }} onPress={() => onOpen(item)}>

            <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: "column", justifyContent: "center" }}>
                    <Ionicons name="pizza" size={45} color={'#B7460C'} />
                </View>
                <View style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    paddingLeft: 8
                }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Categoria</Text>
                    <Text style={{ fontSize: 16 }}>Descrição</Text>
                </View>
                <View style={{ alignSelf: "flex-end", position: 'absolute', right: 5, bottom: 13 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: item.type == 'I' ? '#FF7777' : '#3CB371' }}>{item.type == "I" ? "- " : ""}R$ 100,00</Text>
                </View>

            </View>

        </TouchableOpacity>

    )
    return (

        <View style={{ flex: 1, backgroundColor: '#F5E4CA' }}>

            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={{ flex: 0.97 }}>
                    <Text style={{ marginLeft: 15, color: '#B7460C', fontSize: 25, fontWeight: 'bold', justifyContent: 'flex-start' }}>Lançamentos</Text>
                </View>
                <View>
                    <SideBarNavIcon style={{ justifyContent: 'flex-end' }} navigation={navigation} />
                </View>

            </View>


            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 15, marginRight: 15, marginTop: 10 }}>
                <TouchableOpacity style={{ borderBottomWidth: 3 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Hoje</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Semanal</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Mensal</Text>
                </TouchableOpacity>
            </View>
                      
            <FlatList contentContainerStyle={{ marginTop: 10 }}
                data={trending}
                renderItem={renderItem}
                keyExtractor={item => `${item.id}`}
                showsHorizontalScrollIndicator={false}
            />

            <Modalize ref={modalizeRef} adjustToContentHeight={toggle}><DetailsTransactionModal modalize={modalizeRef}/></Modalize>
            
        </View>
    );
}