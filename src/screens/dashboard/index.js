import React, { useState } from 'react';
import { Text, ScrollView, ImageBackground, Dimensions, View, TextInput, TouchableOpacity, FlatList, Image } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import styles from './style';
import { trendingCurrencies } from '../../constants/dummy'
import SideBarNavIcon from '../../components/SideBarNavIcon';
import { Modalize } from 'react-native-modalize';
import RecordMovimentModal from '../../components/modal/ModalRecordMoviment';
import { LineChart } from "react-native-chart-kit";

export default function Dashboard({ navigation }) {

    const [trending, setTrending] = React.useState(trendingCurrencies);

    const [toggle, setToggle] = useState(true);
    const modalizeRef = React.createRef();;

    const onOpen = () => {
        modalizeRef.current.open();
    };

    const renderItem = ({ item, index }) => (

        <TouchableOpacity
            style={{
                width: 140,
                alignItems: 'center',
                justifyContent: 'center',
                padding: 10,
                // paddingVertical: 20,
                // paddingHorizontal: 20,
                marginLeft: index == 0 ? 10 : 0,
                marginRight: 12,
                borderRadius: 10,
                backgroundColor: '#fff'
            }}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ marginTop: 8 }}>
                    <Ionicons name="pizza" size={30} color={'#B7460C'} />
                </View>
                <View style={{ marginLeft: 15 }}>
                    <Text style={{ fontSize: 18 }}>Comida</Text>
                    <Text style={{ fontSize: 16 }}>Pizza</Text>
                </View>

            </View>
            <View style={{ marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontSize: 18, color: item.type == 'I' ? 'green' : 'red' }}>R$ {item.amount}</Text>
            </View>
        </TouchableOpacity>
    )

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#F5E4CA' }}>

            <View style={{ flex: 0.3,marginBottom:80}}>
                <View style={{ width: '100%', height: 230, backgroundColor: '#F5E4CA' }}>

                    <View style={{ marginTop: 25, justifyContent: 'space-between', flexDirection: 'row-reverse' }}>

                        <SideBarNavIcon style={styles.menu} navigation={navigation} />

                        <TouchableOpacity onPress={() => onOpen()} style={{ width: 45, height: 45, alignItems: 'center', justifyContent: 'center' }}>
                            <Ionicons name="add-circle-outline" size={45} color={'#B7460C'} />
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: 35, height: 35, alignItems: 'center', justifyContent: 'center' }}>
                            <Ionicons name="notifications-outline" size={30} color={'#B7460C'} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ color: '#B7460C', fontSize: 25 }}>Saldo Atual</Text>
                        <Text style={{ color: '#B7460C', fontSize: 30, fontWeight: 'bold' }}>R$ 5.632,89</Text>
                    </View>

                    <View style={{marginTop:10}}>
                        <Text style={{ marginLeft: 10, color: '#B7460C', fontSize: 20 }}>Últimos Lançamentos</Text>
                        <FlatList contentContainerStyle={{ marginTop: 8, flexDirection: 'row' }}
                            data={trending}
                            renderItem={renderItem}
                            keyExtractor={item => `${item.id}`}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </View>

            </View>
            <View style={{ flex: 0.1, flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                <View style={{ justifyContent: 'center', alignItems: 'center', width: '45%', height: '95%', borderWidth: 2, borderRadius: 10, borderColor: '#B7460C' }}>
                    <Text style={{ fontSize: 16 }}>Receita</Text>
                    <Text style={{ fontSize: 20, color: 'green' }}>R$ 3.500,00</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', width: '45%', height: '95%', borderWidth: 2, borderRadius: 10, borderColor: '#B7460C' }}>
                    <Text style={{ fontSize: 16 }}>Despesa</Text>
                    <Text style={{ fontSize: 20, color: 'red' }}>R$ 2.800,00</Text>
                </View>

            </View>

            <View style={{ flex: 0.4, padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: '100%', height: '100%' }}>
                    <LineChart
                        data={{
                            labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho"],
                            datasets: [ 
                                { data: [ 20, 45, 28, 80, 99, 43 ],color: (opacity = 0.4) => `rgba(11,156,49, ${opacity})` },
                                { data: [ 30, 15, 18, 10, 59, 33 ], color: (opacity = 1) => `rgba(255,0,0, ${opacity})`}
                             ],
                            legend: ['Receita', 'Despesa'],
                        }}
                        width={(Dimensions.get("window").width)-20} // from react-native
                        height={200}
                        yAxisLabel="R$"
                        //yAxisSuffix="k"
                        yAxisInterval={1} // optional, defaults to 1
                        chartConfig={{
                            backgroundColor: "#b59564",
                            backgroundGradientFrom: "#b59564",
                            backgroundGradientTo: "#b09c7f",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            
                            propsForDots: {
                                r: "3",
                                strokeWidth: "6",
                                stroke: "#B7460C"
                            }
                        }}
                        bezier
                        style={{
                            
                            borderRadius: 5
                        }}
                    />
                </View>

            </View>



            <Modalize ref={modalizeRef} adjustToContentHeight={toggle}><RecordMovimentModal modalize={modalizeRef} /></Modalize>

        </ScrollView>


    );
}