import React from 'react';
import { Text, ScrollView, ImageBackground, Dimensions, View, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import styles from './style';
import { Checkbox } from 'react-native-paper';


export default function Login({ navigation }) {

  return (

    <ScrollView style={styles.main} showsVerticalScrollIndicator={false}>
      <ImageBackground source={require('../../images/Money.jpg')}
        style={{ height: Dimensions.get('window').height / 2.5 }}>
        <View style={styles.brandView}>
          <Ionicons name="wallet" size={100} color={'#EB822F'} />
          <Text style={styles.brandViewText}>Expense Tracker</Text>
        </View>
      </ImageBackground>
      <View style={styles.bottomView}>
        <View style={{ padding: 40 }}>
          <Text style={{ color: '#B7460C', fontSize: 34 }}>Bem-Vindo</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Register")}>
            <Text style={{ color: '#B7460C', fontSize: 15 }}>Não possui cadastro?
              <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 15 }}> Registre-se</Text>
            </Text>
          </TouchableOpacity>
          <View style={{ marginTop: 50 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 10 }}>
                <Ionicons name="mail" size={20} color={'#EB822F'} />
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  style={styles.input}
                  placeholder="Email"
                  autoCorrect={false}
                  placeholderTextColor='#B7460C'
                // onChangeText={}
                // defaultValue={'a@a.com'}]
                />
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 10 }}>
                <Ionicons name="key" size={20} color={'#EB822F'} />
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  style={styles.input}
                  placeholder="Senha"
                  autoCorrect={false}
                  // onChangeText={}
                  // defaultValue={}
                  secureTextEntry={true}
                  placeholderTextColor='#B7460C'
                />
              </View>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View>
                <Checkbox status='checked' color='#B7460C' />
              </View>
              <View style={{ alignSelf: 'center' }}>
                <Text styles={{ color: '#B7460C', fontSize: 12 }}>Lembrar</Text>
              </View>
            </View>
            <View style={{ alignSelf: 'center' }}>
              <TouchableOpacity>
              <Text styles={{ color: '#B7460C', fontSize: 12 }}>Esqueci a senha</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <TouchableOpacity style={{ height: 40, width: 240, backgroundColor: '#B7460C', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}
              onPress={() => navigation.navigate("Home")}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20 }}>Acessar</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginTop: 10, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ textAlign: 'center' }}>ou faça login com</Text>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
              <TouchableOpacity>
                <Ionicons name="logo-google" size={40} color={'#EB822F'} />
              </TouchableOpacity>
              <TouchableOpacity style={{ marginLeft: 40 }}>
                <Ionicons name="logo-facebook" size={40} color={'#EB822F'} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}