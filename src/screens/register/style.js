import { StyleSheet } from "react-native";

const styles = StyleSheet.create(
  {
    main:{
        flex: 1,
        backgroundColor: '#F5E4CA'
    },
    brandView:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    brandViewText:{
        fontSize:40,
        color:'#EB822F',
        fontWeight:'bold',
        textTransform:'uppercase'
    },
    bottomView:{
        flex:1.5,
        backgroundColor:'#F5E4CA',
        bottom:50,
        borderTopLeftRadius:40,
        borderTopRightRadius:40
        
    },
    input: {
      backgroundColor: '#F5E4CA',
      marginBottom: 15,
      color: '#B7460C',
      fontSize: 15,
      padding: 5,
      borderBottomWidth:1,
      borderColor:'#B7460C'
  },
  forgotPass:{
    flex:1,
    flexDirection:'row'
  }

  }
);

export default styles;
