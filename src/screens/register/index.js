import React from 'react';
import { Text, ScrollView, ImageBackground, Dimensions, View, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import styles from './style';
import { Checkbox } from 'react-native-paper';


export default function Register({ navigation }) {

  return (

    <ScrollView style={styles.main} showsVerticalScrollIndicator={false}>
      <ImageBackground source={require('../../images/Money.jpg')}
        style={{ height: Dimensions.get('window').height / 2.5 }}>
        <View style={styles.brandView}>
          <Ionicons name="wallet" size={100} color={'#EB822F'} />
          <Text style={styles.brandViewText}>Expense Tracker</Text>
        </View>
      </ImageBackground>
      <View style={styles.bottomView}>
        <View style={{ padding: 40 }}>
          <Text style={{ color: '#B7460C', fontSize: 34 }}>Registro</Text>

          <View style={{ marginTop: 50 }}>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 10 }}>
                <Ionicons name="person" size={20} color={'#EB822F'} />
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  style={styles.input}
                  placeholder="Usuário"
                  autoCorrect={false}
                  placeholderTextColor='#B7460C'
                // onChangeText={}
                // defaultValue={'a@a.com'}]
                />
              </View>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 10 }}>
                <Ionicons name="mail" size={20} color={'#EB822F'} />
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  style={styles.input}
                  placeholder="Email"
                  autoCorrect={false}
                  placeholderTextColor='#B7460C'
                // onChangeText={}
                // defaultValue={'a@a.com'}]
                />
              </View>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 10 }}>
                <Ionicons name="key" size={20} color={'#EB822F'} />
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  style={styles.input}
                  placeholder="Senha"
                  autoCorrect={false}
                  // onChangeText={}
                  // defaultValue={}
                  secureTextEntry={true}
                  placeholderTextColor='#B7460C'
                />
              </View>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginTop: 10 }}>
                <Ionicons name="key" size={20} color={'#EB822F'} />
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  style={styles.input}
                  placeholder="Redigite a Senha"
                  autoCorrect={false}
                  // onChangeText={}
                  // defaultValue={}
                  secureTextEntry={true}
                  placeholderTextColor='#B7460C'
                />
              </View>
            </View>
          </View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <TouchableOpacity style={{ height: 40, width: 240, backgroundColor: '#B7460C', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 20 }}>Cadastrar</Text>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    </ScrollView>
  );
}