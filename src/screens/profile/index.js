import React, { useState, useEffect } from 'react';
import { Ionicons } from "@expo/vector-icons";
import {
    View,
    KeyboardAvoidingView,
    Image,
    TextInput,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import styles from './style';
import * as ImagePicker from 'expo-image-picker';
import SideBarNavIcon from '../../components/SideBarNavIcon';

export default function Profile({ navigation }) {
    const [showCancel,setShowCancel] = useState(false);
    const [editableText,setEditableText] = useState(false);
    const [photoUrl,setPhotoUrl]=useState();
    
    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={{flex:0.94}}>
                </View>  
                <View>
                <SideBarNavIcon style={{justifyContent:'flex-end'}} navigation={navigation} />
                </View>
                 
            </View>
            <View style={styles.container}>
                
            <ScrollView style={{marginTop:60}}>
                {/* <View style={styles.header}>
                    <Ionicons name="person-circle-outline" size={55} color={'#B7460C'} />
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color:'#B7460C'}}>Perfil</Text>
                        <Text style={{color:'#B7460C'}}>Aqui pode gerenciar seus dados.</Text>
                    </View>
                </View> */}
                
                <View style={styles.profileImage}>
                    <View>
                        <TouchableOpacity style={styles.pictureAdd} >
                            <View >
                                <Ionicons name="pencil-sharp" size={30} color={'black'} />
                            </View>
                        </TouchableOpacity>
                        <Image source={!photoUrl ? require('../../images/profile-pic2.jpg') :{uri:photoUrl}} style={styles.image} />
                    </View>

                </View>

                <View style={styles.content}>
                    <View>
                        <Text style={{fontWeight:'bold',color:'#B7460C'}}>Nome</Text>
                        <TextInput 
                        style={styles.input}
                        placeholder="Seu Nome"
                        autoCorrect={false}
                        editable={editableText}
                        // onChangeText={name => setName(name)}
                        defaultValue="Rodrigo Santos"/>

                        <Text style={{fontWeight:'bold',color:'#B7460C'}}>Email</Text>
                        <TextInput 
                        style={styles.input}
                        placeholder="Seu Email"
                        autoCorrect={false}
                        editable={editableText}
                        defaultValue="rodrigo.santos@hotmail.com"/>

                        <Text style={{fontWeight:'bold',color:'#B7460C'}}>Telefone</Text>
                        <TextInput 
                        style={styles.input}
                        placeholder="Seu Telefone"
                        autoCorrect={false}
                        editable={editableText}
                        defaultValue="(11) 91234-5678"/>
                    </View>

                    <View style={styles.buttons}>
                        <TouchableOpacity style={styles.button}>
                        <Text style={{color:'#fff', fontSize:16}}>Editar</Text>
                        </TouchableOpacity>
                        {showCancel ?
                        (<TouchableOpacity style={styles.buttonCancel} onPress={()=>cancelEditProfile()}>
                        <Text style={{color:'#fff', fontSize:16}}>Cancelar</Text>
                        </TouchableOpacity>)
                        :null
                        }   
                    
                     </View>
                    
                </View>
                </ScrollView>   
            </View>
            
        </KeyboardAvoidingView>
    );
}