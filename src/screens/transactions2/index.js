import React, { useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import SideBarNavIcon from '../../components/SideBarNavIcon';
import { Modalize } from 'react-native-modalize';
import DetailsTransactionModal from '../../components/modal/ModalDetailsTransaction';
import { Agenda } from 'react-native-calendars';
import moment from 'moment';
import { LocaleConfig } from 'react-native-calendars';
import RecordMovimentModal from '../../components/modal/ModalRecordMoviment';

export default function Transactions2({ navigation }) {

    const [calendarOpen, setCalendarOpen] = useState(false);
    const [modal, setModal] = useState(null);
    // const [date, setDate] = useState(moment().format('yyyy-MM-DD'));
    const [date, setDate] = useState('2021-08-07');
    const [toggle, setToggle] = useState(true);
    const dataItems = {
        '2021-08-07': [{ name: 'Compra de Presente A', conta: 'Bradesco', value: 'R$ 100,00' }, { name: 'Compra de Presente B', conta: 'NuBank', value: 'R$ 200,00' }]
    }

    LocaleConfig.locales["pt-br"] = {
        monthNames: [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"
        ],
        monthNamesShort: [
            "Jan.",
            "Fev.",
            "Mar",
            "Abr",
            "Mai",
            "Jun",
            "Jul.",
            "Ago",
            "Set.",
            "Out.",
            "Nov.",
            "Dez."
        ],
        dayNames: [
            "Domingo",
            "Segunda",
            "Terça",
            "Quarta",
            "Quinta",
            "Sexta",
            "Sábado"
        ],
        dayNamesShort: ["Dom.", "Seg.", "Ter.", "Qua.", "Qui.", "Sex.", "Sáb."]
    };

    LocaleConfig.defaultLocale = "pt-br";

    const modalizeRef = React.createRef();;
    const onOpen = (modalOpen) => {
        setModal(modalOpen)
        modalizeRef.current.open();
    };

    function renderItem(item) {
        console.log("render")

        return (
            <TouchableOpacity onPress={() => (onOpen('detailsTransaction'))} style={{ marginTop: 30 }}>
                <View style={styles.item}>
                    <View>
                        <Ionicons name="pizza" size={30} color={'#B7460C'} />
                    </View>
                    <View>
                        <Text style={{ fontSize: 16 }}>{item.name}</Text>
                        <Text style={{ fontSize: 12 }}>{item.conta}</Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: 16, color: 'green' }}>{item.value}</Text>
                    </View>
                </View>
            </TouchableOpacity>



        );
    }

    function renderEmptyData(item) {

        return (
            <View style={styles.emptyItem}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Ionicons name="alert" size={30} color={'#B7460C'} />
                    </View>
                    <View>
                        <Text style={{ fontSize: 18 }}>Sem Lançamentos para este dia.</Text>
                    </View>
                    <TouchableOpacity onPress={() => onOpen('moviment')}>
                        <Ionicons name="add-circle" size={30} color={'#B7460C'} />
                    </TouchableOpacity>
                </View>
            </View>



        );
    }


    return (

        <View style={{ flex: 1, backgroundColor: '#F5E4CA' }}>

            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={{ flex: 0.97 }}>
                    <Text style={{ marginLeft: 15, color: '#B7460C', fontSize: 25, fontWeight: 'bold', justifyContent: 'flex-start' }}>Lançamentos</Text>
                </View>
                <View>
                    <SideBarNavIcon style={{ justifyContent: 'flex-end' }} navigation={navigation} />
                </View>

            </View>


            <View style={{ flex: 1 }}>
                <View style={styles.dateViewStyle}>
                    <TouchableOpacity
                        style={styles.dateButtonStyle}
                    //onPress = {() => openCalendar ? setOpenCalendar(false) : setOpenCalendar(true) }
                    >
                        <Text style={styles.dateStyle}>{moment(date).format('MMMM/yyyy')}</Text>
                    </TouchableOpacity>
                </View>
                <Agenda
                    hideKnob={false}
                    showClosingKnob={true}
                    onCalendarToggled={(calendarOpened) => setCalendarOpen(calendarOpened)}
                    renderKnob={() => {
                        return (
                        <View>
                            {calendarOpen  && (<Text style={{fontSize:16, color:'#B7460C',textDecorationLine:'underline'}}>Fechar</Text>)}
                            {!calendarOpen && (<Text style={{fontSize:14, color:'#B7460C',textDecorationLine:'underline'}}>Expandir</Text>)}   
                        </View>
                        );}}
                    selected={date}
                    pastScrollRange={12}
                    futureScrollRange={3}
                    items={dataItems}
                    renderItem={(item) => renderItem(item)}
                    renderEmptyData={() => renderEmptyData()}
                    hideArrows={false}
                    onDayPress={(day) => {setDate(day.dateString)}}
                    // rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
                    theme={{
                        backgroundColor: '#F5E4CA',
                        calendarBackground: '#F5E4CA',
                        textSectionTitleColor: '#A9A9A9',
                    }}

                />
            </View>
            <Modalize ref={modalizeRef} adjustToContentHeight={toggle}>
                {modal == 'detailsTransaction' && (<DetailsTransactionModal modalize={modalizeRef} />)}
                {modal == 'moviment' && (<RecordMovimentModal modalize={modalizeRef} />)}
                {modal == null && (null)}

            </Modalize>
        </View>
    );
}
const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 50,
        borderLeftColor: 'purple',
        borderLeftWidth: 5,
        flex: 1,
        borderRadius: 5,
        padding: 10,
        width: '95%',
        height: 60
    },
    emptyItem: {
        backgroundColor: 'white',
        height: 50,
        borderLeftColor: '#A9A9A9',
        borderLeftWidth: 5,
        borderRadius: 5,
        padding: 10,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 35,
        height: 60
    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    },
    dateViewStyle: {
        flexDirection: "row",
        justifyContent: "center",
        height: "auto"
    },
    dateStyle: {
        color: "#A9A9A9",
        fontSize: 18,
        padding: 10,
        margin: 5,
        borderRadius: 5
    },
    viewStyle: {
        flexDirection: "row",
        justifyContent: "center",
        padding: 5,
        marginTop: 30,
        height: 50
    },
    textStyle: {
        fontSize: 18,
        margin: 5
    }
});