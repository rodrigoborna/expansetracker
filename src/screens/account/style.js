import { StyleSheet } from "react-native";

const styles = StyleSheet.create(
  {
    content: {
      padding: 20,
    },
  
    content__icon: {
      width: 32,
      height: 32,
  
      marginBottom: 20,
    },
  
    content__subheading: {
      marginBottom: 10,
  
      fontSize: 16,
      fontWeight: '600',
      color: '#B7460C',
    },
  
    content__heading: {
      fontSize: 20,
      fontWeight: '600',
      color: '#333',
      paddingLeft:5,
      fontWeight:'bold',
      marginBottom:10
    },
  
    content__description: { 
      fontSize: 15,
      fontWeight: '200',
      lineHeight: 22,
      color: '#666',
      
    },
  
    content__input: {
      width: '100%',
      height:50,
      borderWidth: 1,
      borderColor: 'transparent',
      borderColor: 'black',
      borderRadius: 10,
      borderWidth:1,
      paddingLeft:5,
      marginBottom:20,
      fontSize:20
    },
  
    content__buttonEdit: {
      paddingVertical: 15,
      width: '45%',
      backgroundColor: '#333',
      borderRadius: 6,
    },
    content__buttonDelete: {
      paddingVertical: 15,
      width: '45%',
      backgroundColor: '#FF7777',
      borderRadius: 6,
    },
  
    content__buttonText: {
      color: '#fff',
      fontSize: 15,
      fontWeight: '600',
      textAlign: 'center',
    },

  }
);

export default styles;