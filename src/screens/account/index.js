import React, { useState } from 'react';
import { Text, View, TouchableOpacity,ScrollView } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import SideBarNavIcon from '../../components/SideBarNavIcon';
import RecordAccountModal from '../../components/modal/ModalRecordCategory';
import { Modalize } from 'react-native-modalize';

export default function Account({ navigation }) {
    const [toggle, setToggle] = useState(true);
    const modalizeRef = React.createRef();;
    const onOpen = () => {
        
        modalizeRef.current.open();
    };

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#F5E4CA'}}>
            
            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={{ flex: 0.97 }}>
                    <Text style={{ marginLeft: 15, color: '#B7460C', fontSize: 25, fontWeight: 'bold', justifyContent: 'flex-start' }}>Contas</Text>
                </View>
                <View>
                    <SideBarNavIcon style={{ justifyContent: 'flex-end' }} navigation={navigation} />
                </View>
            </View>

            <View style={{ padding: 15}}>
                <TouchableOpacity style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                            <Text style={{fontSize:16}}>Conta: Bradesco</Text>
                            <Text style={{fontSize:16}}>Saldo Atual: R$ 52.654,78</Text>
                            
                
                </TouchableOpacity>           
            </View>
            <View style={{ padding: 15 }}>
                <TouchableOpacity style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 10,
                            borderWidth: 3,
                            borderColor: '#B7460C'
                        }}>
                             <Text style={{fontSize:16}}>Conta: NuBank</Text>
                            <Text style={{fontSize:16}}>Saldo Atual: R$ 52.654,78</Text>
                
                </TouchableOpacity>           
            </View>


            <Modalize ref={modalizeRef} adjustToContentHeight={toggle}>
                <RecordAccountModal modalize={modalizeRef} />
            </Modalize>
        </ScrollView>
    );
}