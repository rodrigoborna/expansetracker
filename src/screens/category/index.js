import React, { useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from "@expo/vector-icons"
import SideBarNavIcon from '../../components/SideBarNavIcon';
import RecordCategoryModal from '../../components/modal/ModalRecordCategory';
import { Modalize } from 'react-native-modalize';

export default function Category({ navigation }) {
    const [toggle, setToggle] = useState(true);
    const modalizeRef = React.createRef();;
    const onOpen = () => {
        
        modalizeRef.current.open();
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#F5E4CA'}}>

            <View style={{ marginTop: 25, flexDirection: 'row' }}>
                <View style={{ flex: 0.97 }}>
                    <Text style={{ marginLeft: 15, color: '#B7460C', fontSize: 25, fontWeight: 'bold', justifyContent: 'flex-start' }}>Categorias</Text>
                </View>
                <View>
                    <SideBarNavIcon style={{ justifyContent: 'flex-end' }} navigation={navigation} />
                </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 0.2, flexDirection: 'row' }}>

                    <View style={{ borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" ,width: '50%',alignItems: 'center', justifyContent: 'center' }}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria A</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                    <View style={{borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" , width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria B</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                </View>




                <View style={{ flex: 0.2, flexDirection: 'row' }}>

                    <View style={{ borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" ,width: '50%',alignItems: 'center', justifyContent: 'center' }}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria C</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                    <View style={{borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" , width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria D</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                </View>
                <View style={{ flex: 0.2, flexDirection: 'row' }}>

                    <View style={{ borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" ,width: '50%',alignItems: 'center', justifyContent: 'center' }}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria E</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                    <View style={{borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" , width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria F</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                </View>
                <View style={{ flex: 0.2, flexDirection: 'row' }}>

                    <View style={{ borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" ,width: '50%',alignItems: 'center', justifyContent: 'center' }}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria G</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                    <View style={{borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" , width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria H</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                </View>
                <View style={{ flex: 0.2, flexDirection: 'row' }}>

                    <View style={{ borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" ,width: '50%',alignItems: 'center', justifyContent: 'center' }}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria I</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                    <View style={{borderLeftWidth:1,borderTopWidth:1,borderColor:"#808080" , width: '50%', alignItems: 'center', justifyContent: 'center'}}>
                        
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Ionicons name="bookmark" size={35} color={'#B7460C'} />
                                <Text style={{ fontSize: 20 }}>Categoria J</Text>
                                <Text style={{ fontSize: 14 }}>R$ 123,56</Text>
                                <View style={{width:50,heigth:5,borderRadius:12,backgroundColor:'purple'}}><Text style={{height:5}}> </Text></View>
                            </View>
                        
                    </View>

                </View>



            </View>


            <Modalize ref={modalizeRef} adjustToContentHeight={toggle}>
                <RecordCategoryModal modalize={modalizeRef} />
            </Modalize>
        </View>
    );
}